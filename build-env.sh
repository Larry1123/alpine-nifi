#!/bin/sh

export NIFI_VERSION=1.20.0
export NIFI_DOWNLOAD_CACHE=$(pwd)/.cache
export MIRROR="$(curl -s 'https://www.apache.org/dyn/closer.cgi' | grep -o '<strong>[^<]*</strong>' | sed 's/<[^>]*>//g' | head -1)"

export BASE_IMAGE=registry.gitlab.com/larry1123/alpine-nifi
export IMAGE_TAG="${NIFI_VERSION}$(echo "-$(git rev-parse --short HEAD)")$(git diff-index --quiet HEAD -- || echo -dirty)"
export DEPLOY_TAG=${NIFI_VERSION}

BASE_REF=${BASE_IMAGE}:${IMAGE_TAG}
CACHE_REF=${BASE_IMAGE}/cache:${NIFI_VERSION}
DEPLOY_REF=${BASE_IMAGE}:${DEPLOY_TAG}

dockerBuild() {
  docker buildx build -t "${BASE_REF}" --build-arg BUILD_DATE="$(date -u +"%Y-%m-%dT%H:%M:%SZ")" --build-arg VCS_REF="$(git rev-parse --short HEAD)" --build-arg NIFI_VERSION=${NIFI_VERSION} --build-arg MIRROR=${MIRROR} --pull --load .
  echo "${BASE_REF}"
}

dockerTag() {
  docker tag ${BASE_REF} $1
}

dockerDeploy() {
  dockerTag ${DEPLOY_REF}
  docker push ${DEPLOY_REF}

  DOCKERHUB_IMAGE=larry1123/alpine-nifi

  dockerTag "${DOCKERHUB_IMAGE}:${IMAGE_TAG}"
  dockerTag "${DOCKERHUB_IMAGE}:${DEPLOY_TAG}"
  docker push "${DOCKERHUB_IMAGE}:${IMAGE_TAG}"
  docker push "${DOCKERHUB_IMAGE}:${DEPLOY_TAG}"
}
