#!/bin/sh -e

if [ -d "${NIFI_HOME}/lib-ex" ]; then
    ln -snf ${NIFI_HOME}/lib-ex/* ${NIFI_HOME}/lib
fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- start.sh "$@"
fi

# allow the container to be started with `--user`
if [ "$1" = 'nifi.sh' -a "$(id -u)" = '0' ]; then
	chown -R nifi:nifi "${NIFI_HOME}"
	exec su-exec nifi "$0" "$@"
fi

exec "$@"
