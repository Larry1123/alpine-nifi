#!/bin/sh
set -ex
MIRROR=${MIRROR:-https://archive.apache.org/dist}
NIFI_URI_BASE=/nifi/${NIFI_VERSION}
NIFI_BINARY_NAME=nifi-${NIFI_VERSION}-bin.zip
NIFI_TOOLKIT_NAME=nifi-toolkit-${NIFI_VERSION}-bin.zip

downloadVerifyFiles() {
  curl -Lf --proto =https "https://archive.apache.org/dist${NIFI_URI_BASE}/${1}.asc" -o "${NIFI_DOWNLOAD_CACHE}/${1}.asc"
  curl -Lf --proto =https "https://archive.apache.org/dist${NIFI_URI_BASE}/${1}.sha1" -o "${NIFI_DOWNLOAD_CACHE}/${1}.sha1" || true
  curl -Lf --proto =https "https://archive.apache.org/dist${NIFI_URI_BASE}/${1}.sha256" -o "${NIFI_DOWNLOAD_CACHE}/${1}.sha256" || true
  curl -Lf --proto =https "https://archive.apache.org/dist${NIFI_URI_BASE}/${1}.sha512" -o "${NIFI_DOWNLOAD_CACHE}/${1}.sha512" || true
}

downloadBinary() {
  if test -f "${NIFI_DOWNLOAD_CACHE}/${1}";
  then
    curl -vLf "${MIRROR}${NIFI_URI_BASE}/${1}" -z "${NIFI_DOWNLOAD_CACHE}/${1}" -o "${NIFI_DOWNLOAD_CACHE}/${1}"
  else
    curl -vLf "${MIRROR}${NIFI_URI_BASE}/${1}" -o "${NIFI_DOWNLOAD_CACHE}/${1}"
  fi
}

verifyFile() {
  # verify signing and hashes
  gpg --verify "${NIFI_DOWNLOAD_CACHE}/${1}.asc" "${NIFI_DOWNLOAD_CACHE}/${1}"
  if test -f "${NIFI_DOWNLOAD_CACHE}/${1}.sha1"; then
    echo "$(cat "${NIFI_DOWNLOAD_CACHE}/${1}.sha1")  ${NIFI_DOWNLOAD_CACHE}/${1}" | sha1sum -c -
  fi
  if test -f "${NIFI_DOWNLOAD_CACHE}/${1}.sha256"; then
    echo "$(cat "${NIFI_DOWNLOAD_CACHE}/${1}.sha256")  ${NIFI_DOWNLOAD_CACHE}/${1}" | sha256sum -c -
  fi
  if test -f "${NIFI_DOWNLOAD_CACHE}/${1}.sha512"; then
    echo "$(cat "${NIFI_DOWNLOAD_CACHE}/${1}.sha512")  ${NIFI_DOWNLOAD_CACHE}/${1}" | sha512sum -c -
  fi
}

cleanup() {
  rm -f "${NIFI_DOWNLOAD_CACHE}/${1}.asc"
  rm -f "${NIFI_DOWNLOAD_CACHE}/${1}.sha1"
  rm -f "${NIFI_DOWNLOAD_CACHE}/${1}.sha256"
  rm -f "${NIFI_DOWNLOAD_CACHE}/${1}.sha512"
}

mkdir -p "${NIFI_DOWNLOAD_CACHE}"

# Download gpg repo keys and nifi hashes
curl -Lf --proto =https https://dist.apache.org/repos/dist/release/nifi/KEYS -o "${NIFI_DOWNLOAD_CACHE}/nifi-keys.txt"
gpg --import "${NIFI_DOWNLOAD_CACHE}/nifi-keys.txt"

downloadVerifyFiles "${NIFI_BINARY_NAME}"
downloadVerifyFiles "${NIFI_TOOLKIT_NAME}"

downloadBinary "${NIFI_BINARY_NAME}"
downloadBinary "${NIFI_TOOLKIT_NAME}"

verifyFile "${NIFI_BINARY_NAME}"
verifyFile "${NIFI_TOOLKIT_NAME}"

# clean up
rm -f "${NIFI_DOWNLOAD_CACHE}/nifi-keys.txt"
cleanup "${NIFI_BINARY_NAME}"
cleanup "${NIFI_TOOLKIT_NAME}"
