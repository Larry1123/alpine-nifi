# syntax = docker/dockerfile:1.5

FROM alpine:3.17 as nifi-download

ARG NIFI_VERSION=1.20.0
ARG MIRROR=${MIRROR:-https://archive.apache.org/dist}

ENV NIFI_HOME=/opt/nifi
ENV NIFI_TOOLKIT_HOME=/opt/toolkit
ENV NIFI_DOWNLOAD_CACHE=/root/.cache/nifi

RUN --mount=type=cache,sharing=locked,target=/var/cache/apk/ apk add --update \
        curl \
        gnupg \
        openjdk17-jre-headless \
        git \
    ;

COPY download-nifi.sh download-nifi.sh
RUN --mount=type=cache,target=/root/.cache/nifi ash download-nifi.sh

COPY extract-nifi.sh extract-nifi.sh
RUN --mount=type=cache,target=/root/.cache/nifi ash extract-nifi.sh

RUN git clone --depth 1 --filter=blob:none --sparse https://github.com/apache/nifi.git --branch rel/nifi-${NIFI_VERSION} ${NIFI_HOME}/nifi-${NIFI_VERSION}-scripts
WORKDIR ${NIFI_HOME}/nifi-${NIFI_VERSION}-scripts
RUN git sparse-checkout set nifi-docker

FROM alpine:3.17

ARG UID=1000
ARG GID=1000
ARG NIFI_VERSION=1.20.0

ENV NIFI_HOME=/opt/nifi
ENV NIFI_TOOLKIT_HOME=/opt/toolkit
ENV NIFI_PID_DIR=${NIFI_HOME}/run
ENV NIFI_LOG_DIR=${NIFI_HOME}/logs

RUN --mount=type=cache,sharing=locked,target=/var/cache/apk/ apk add --update \
        dumb-init \
        su-exec \
        ca-certificates \
        openjdk17-jre-headless \
        coreutils \
    ;

RUN addgroup -S -g ${GID} nifi && adduser -S -g nifi -u ${UID} nifi

COPY --from=nifi-download --chown=nifi:nifi ${NIFI_HOME}/nifi-${NIFI_VERSION} ${NIFI_HOME}
COPY --from=nifi-download --chown=nifi:nifi ${NIFI_TOOLKIT_HOME}/nifi-toolkit-${NIFI_VERSION} ${NIFI_TOOLKIT_HOME}
COPY --from=nifi-download --chown=nifi:nifi ${NIFI_HOME}/nifi-${NIFI_VERSION}-scripts/nifi-docker/dockerhub/sh ${NIFI_HOME}/scripts

ENV PATH=${NIFI_HOME}/bin:${NIFI_HOME}/scripts:${NIFI_TOOLKIT_HOME}/bin:${PATH}

# These are the volumes (in order) for the following:
# 1) user access and flow controller history
# 2) FlowFile attributes and current state in the system
# 3) content for all the FlowFiles in the system
# 4) information related to Data Provenance
# You can find more information about the system properties here - https://nifi.apache.org/docs/nifi-docs/html/administration-guide.html#system_properties
VOLUME [ \
    "${NIFI_HOME}/database_repository", \
    "${NIFI_HOME}/flowfile_repository", \
    "${NIFI_HOME}/content_repository", \
    "${NIFI_HOME}/provenance_repository", \
    "${NIFI_HOME}/state", \
    "${NIFI_HOME}/conf", \
    "${NIFI_LOG_DIR}" \
]

# Web HTTP(s) & Socket Site-to-Site Ports
EXPOSE 8080 8443 10000 8000

WORKDIR ${NIFI_HOME}
COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["dumb-init", "--", "docker-entrypoint.sh"]
CMD ["start.sh"]

# Build-time metadata as defined at http://label-schema.org
ARG BUILD_DATE
ARG VCS_REF
LABEL org.label-schema.build-date=${BUILD_DATE} \
      org.label-schema.name="larry1123/alpine-nifi" \
      org.label-schema.description="An alpine based build of Apache Nifi" \
      org.label-schema.url="https://gitlab.com/Larry1123/alpine-nifi" \
      org.label-schema.vcs-ref=${VCS_REF} \
      org.label-schema.vcs-url="https://gitlab.com/Larry1123/alpine-nifi" \
      org.label-schema.vendor="Larry1123" \
      org.label-schema.version=${NIFI_VERSION} \
      org.label-schema.schema-version="1.0"
