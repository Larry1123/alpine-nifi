#!/bin/sh
set -ex

NIFI_BINARY_NAME=nifi-${NIFI_VERSION}-bin.zip
NIFI_TOOLKIT_NAME=nifi-toolkit-${NIFI_VERSION}-bin.zip

mkdir -p "${NIFI_HOME}"
mkdir -p "${NIFI_TOOLKIT_HOME}"
unzip "${NIFI_DOWNLOAD_CACHE}/${NIFI_BINARY_NAME}" -d "${NIFI_HOME}"
unzip "${NIFI_DOWNLOAD_CACHE}/${NIFI_TOOLKIT_NAME}" -d "${NIFI_TOOLKIT_HOME}"

# Clear nifi-env.sh in favour of configuring all environment variables in the Dockerfile
echo "#!/bin/sh\n" > ${NIFI_HOME}/nifi-${NIFI_VERSION}/bin/nifi-env.sh

# Brand the UI
sed -i -e "s|^nifi.ui.banner.text=.*$|nifi.ui.banner.text=Apache NiFi ${NIFI_VERSION}|" "${NIFI_HOME}/nifi-${NIFI_VERSION}/conf/nifi.properties"

# Create volume directories
mkdir -p "${NIFI_HOME}/nifi-${NIFI_VERSION}/database_repository"
mkdir -p "${NIFI_HOME}/nifi-${NIFI_VERSION}/flowfile_repository"
mkdir -p "${NIFI_HOME}/nifi-${NIFI_VERSION}/content_repository"
mkdir -p "${NIFI_HOME}/nifi-${NIFI_VERSION}/provenance_repository"

echo `du -sh ${NIFI_HOME}/nifi-${NIFI_VERSION}`
echo `du -sh ${NIFI_HOME}/nifi-toolkit-${NIFI_VERSION}`
