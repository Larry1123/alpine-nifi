larry1123/alpine-nifi
=======

[![](https://images.microbadger.com/badges/version/larry1123/alpine-nifi.svg)](https://microbadger.com/images/larry1123/alpine-nifi) [![](https://images.microbadger.com/badges/image/larry1123/alpine-nifi.svg)](https://microbadger.com/images/larry1123/alpine-nifi)    

An alpine based build of [Apache NiFi](https://nifi.apache.org/).

Usage
-------------


Build Info
-------------
The project's home is on [gitlab](https://gitlab.com/larry1123/alpine-nifi/).  
Issues and merge request should be made there.  
Each Image uses [dumb-init](https://github.com/Yelp/dumb-init).  
This project is hosted on [docker hub](https://hub.docker.com/r/larry1123/alpine-nifi/) also, it is not updated during the build pipeline yet.  

Thanks
-------------
[Apache](https://apache.org/) They maintain so many good projects.  
[Apache NiFi](https://nifi.apache.org/) NiFi is nearly magic.  

How to build
-------------
This repo is setup to be built with gitlab's ci runner.
